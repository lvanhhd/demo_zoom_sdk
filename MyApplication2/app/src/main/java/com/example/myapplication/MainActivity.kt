package com.example.myapplication;

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.databinding.ActivityMainBinding
import com.google.android.material.textfield.TextInputEditText
import us.zoom.sdk.*


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initializeSdk(this)
        initViews()
    }

    /**
     * Initialize the SDK with your credentials. This is required before accessing any of the
     * SDK's meeting-related functionality.
     */
    private fun initializeSdk(context: Context) {
        val sdk = ZoomSDK.getInstance()
        // TODO: Do not use hard-coded values for your key/secret in your app in production!
        val params = ZoomSDKInitParams().apply {
            appKey = "w8Na0kKg8gY8oJrJuMgjHmJ5pUTvHIJ7uvKD" // TODO: Retrieve your SDK key and enter it here
            appSecret = "sJUvD9v4l7i0rbM8m9jZPDrE7GPoH5Ccrtoe" // TODO: Retrieve your SDK secret and enter it here
            domain = "zoom.us"
            enableLog = true // Optional: enable logging for debugging
        }
        // TODO (optional): Add functionality to this listener (e.g. logs for debugging)
        val listener = object : ZoomSDKInitializeListener {
            /**
             * If the [errorCode] is [ZoomError.ZOOM_ERROR_SUCCESS], the SDK was initialized and can
             * now be used to join/start a meeting.
             */
            override fun onZoomSDKInitializeResult(errorCode: Int, internalErrorCode: Int) = Unit
            override fun onZoomAuthIdentityExpired() = Unit
        }
        sdk.initialize(context, listener, params)
    }
    private fun initViews() {
        binding.joinButton.setOnClickListener {
            createJoinMeetingDialog()
        }
        binding.startButton.setOnClickListener {
            startMeetingZak(this)
        }
    }
    /**
     * Join a meeting without any login/authentication with the meeting's number & password
     */
    private fun joinMeeting(context: Context, meetingNumber: String, pw: String) {
        val meetingService = ZoomSDK.getInstance().meetingService
        val options = JoinMeetingOptions()
        options.no_titlebar = true
        options.no_invite = true
        options.no_meeting_end_message = true
        options.no_chat_msg_toast = true
        options.no_driving_mode = true
        val params = JoinMeetingParams().apply {
            displayName = "Anh 2" // TODO: Enter your name
            meetingNo = meetingNumber
            password = pw
        }
        meetingService.joinMeetingWithParams(context, params, options)
    }
    private fun startMeetingZak(context: Context) {
        val meetingService = ZoomSDK.getInstance().meetingService
        val options = StartMeetingOptions()
//        options.no_driving_mode = true
//        //		opts.no_meeting_end_message = true;
        options.no_titlebar = true
//        options.no_bottom_toolbar = true
        options.no_invite = true
        options.no_driving_mode = true
        val params = StartMeetingParamsWithoutLogin().apply {
            displayName = "Anh"
            userId = "5nK5qVf8SmmOmJakpw0OCw"
            zoomAccessToken = "eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6IjVuSzVxVmY4U21tT21KYWtwdzBPQ3ciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEsIndjZCI6InVzMDQiLCJjbHQiOjAsImV4cCI6MTY2ODU5MTk0MSwiaWF0IjoxNjY4NTkxNjQxLCJhaWQiOiJ4dDlEZk9kcVJ1TzdZc19LODVfVnBnIiwiY2lkIjoiIn0.zzRFdJkROfRKpUQl37Leya1RWKPiBrohjKOpczYQbKs"
        }
        meetingService.startMeetingWithParams(context, params, options)
    }
    /**
     * Prompt the user to input the meeting number and password and uses the Zoom SDK to join the
     * meeting.
     */
    private fun createJoinMeetingDialog() {
        AlertDialog.Builder(this)
            .setView(R.layout.dialog_main)
            .setPositiveButton("Join") { dialog, _ ->
                dialog as AlertDialog
                val numberInput = dialog.findViewById<TextInputEditText>(R.id.meeting_no_input)
                val passwordInput = dialog.findViewById<TextInputEditText>(R.id.password_input)
                val meetingNumber = numberInput?.text?.toString()
                val password = passwordInput?.text?.toString()
                meetingNumber?.takeIf { it.isNotEmpty() }?.let { meetingNo ->
                    password?.let { pw ->
                        joinMeeting(this@MainActivity, meetingNo, pw)
                    }
                }
                dialog.dismiss()
            }
            .show()
    }
    private fun customizeUI (){
        ZoomSDK.getInstance().meetingSettingsHelper.isCustomizedMeetingUIEnabled = true
        val mInMeetingService = ZoomSDK.getInstance().inMeetingService

    }
}