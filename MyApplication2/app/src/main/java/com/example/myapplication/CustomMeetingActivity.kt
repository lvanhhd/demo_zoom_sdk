package com.example.myapplication

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.myapplication.databinding.ActivityCustomMeetingBinding
import com.herewhite.sdk.*
import com.herewhite.sdk.domain.MemberState
import com.herewhite.sdk.domain.Promise
import com.herewhite.sdk.domain.Region
import com.herewhite.sdk.domain.SDKError
import io.agora.board.fast.FastboardView
import io.agora.board.fast.model.FastRegion
import io.agora.board.fast.model.FastRoomOptions
import io.agora.rtc2.*
import us.zoom.sdk.*
import us.zoom.sdk.InMeetingAudioController.MobileRTCMicrophoneError
import us.zoom.sdk.InMeetingChatController.MobileRTCWebinarPanelistChatPrivilege
import us.zoom.sdk.InMeetingServiceListener.RecordingStatus


class CustomMeetingActivity : MeetingActivity() {
    private lateinit var binding: ActivityCustomMeetingBinding
    private var inMeetingService: InMeetingService? = null
    private val appIdWhiteBoard: String = "d0O6sF87Ee2BimmL04v3Jg/TbYqhlJXaTyKMQ"
    private val uuid: String = "0a117460601111ed8e58650184ae5e98"
    private val roomToken: String =
        "NETLESSROOM_YWs9MXZlakRxSGJkQkhHNll0TCZleHBpcmVBdD0xNjY3OTg5ODY2MDc2Jm5vbmNlPTE2Njc5ODYyNjYwNzYwMCZyb2xlPTAmc2lnPTk0NzRjMjlmODBmN2VmYjc1ZGYyODdlN2EyYmUwYTIyMzNmYzVmNzk3NGYwYzE3ZTU0YTM2MDA5YjUyYjAwZjgmdXVpZD0wYTExNzQ2MDYwMTExMWVkOGU1ODY1MDE4NGFlNWU5OA"
    private val uidWhiteBoard: String = ""

    private val appId = "ea391739be0d4a2e84fc5ac2d0291c40"


    // Fill the channel name.
    private val channelName = "demoapp"

    private val uid = 5
//    private val uid = 1

    // Fill the temp token generated on Agora Console.
    private val token =
        "007eJxTYDgwSXJrQLG7troi25eJjy1dWe82q5+yjHfPbL2/fsbEpV0KDKmJxpaG5saWSakGKSaJRqkWJmnJponJRikGRpaGySYGirY1yQ2BjAz7dp1gYIRCEJ+dISU1Nz+xoICBAQBJ9B/J\n" +
                "\n"
    // Track the status of your connection
    private var isJoined = false

    // Agora engine instance
    private var agoraEngine: RtcEngine? = null

    private var isMute = true
    private var isVolumeZero = true
    // UI elements

    // Create a WhiteboardView object to implement the whiteboard view
//    lateinit var whiteboardView: WhiteboardView
    lateinit var whiteboardView: FastboardView

    // Create a WhiteSdkConfiguration object to configure the App Identifier and log settings
    val sdkConfiguration = WhiteSdkConfiguration(appIdWhiteBoard, true)

    // Set the data center as Silicon Valley, US
    // Create a RoomParams object to set room parameters used in joining a room.
    // If you use versions earlier than v2.15.0, do not pass in uid.
    val roomParams = RoomParams(uuid, roomToken, uidWhiteBoard)


    override fun getLayout(): Int {
        return R.layout.activity_custom_meeting
    }

    override fun isAlwaysFullScreen(): Boolean {
        return false
    }

    override fun isSensorOrientationEnabled(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomMeetingBinding.inflate(layoutInflater)
        sdkConfiguration.region = Region.us
        //zoom sdk
        initListeners()
        //agora
        whiteboardView = findViewById(R.id.white)
        // Create a WhiteSdk object to initialize the whiteboard SDK
//        val whiteSdk = WhiteSdk(whiteboardView, this, sdkConfiguration)
//        // Join a room
//        whiteSdk.joinRoom(roomParams, object : Promise<Room> {
//            override fun then(wRoom: Room) {
//                val memberState = MemberState()
//                // Set the tool to pencil
//                memberState.currentApplianceName = "pencil"
//                // Set the color tor red
//                memberState.strokeColor = intArrayOf(255, 0, 0)
//                // Assign the set-up tool to the current user
//                wRoom.memberState = memberState
//            }
//
//            override fun catchEx(t: SDKError) {
//                val o: Any? = t.message
//                Log.i("showToast", o.toString())
//                Toast.makeText(this@CustomMeetingActivity, o.toString(), Toast.LENGTH_SHORT).show()
//            }
//        })
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        setupFastboard()

        findViewById<TextView>(R.id.btnWhiteBoard).setOnClickListener {
            whiteboardView.visibility =
                if (whiteboardView.visibility == View.GONE) View.VISIBLE else View.GONE
        }

        //voice call
        setupVoiceSDKEngine()
        findViewById<TextView>(R.id.btnExport).setOnClickListener {
            isMute = !isMute
            //mute local
            agoraEngine?.muteLocalAudioStream(isMute)
        }
        findViewById<TextView>(R.id.btnStop).setOnClickListener {
            isVolumeZero = !isVolumeZero
            //turn on/off my volume
            agoraEngine?.adjustPlaybackSignalVolume(if (isVolumeZero) 0 else 100)
        }

    }

    private val mRtcEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {
        override fun onUserJoined(uid: Int, elapsed: Int) {
        }

        override fun onJoinChannelSuccess(channel: String, uid: Int, elapsed: Int) {
            // Successfully joined a channel
            isJoined = true
            showMessage("Joined Channel $channel")
        }

        override fun onUserOffline(uid: Int, reason: Int) {
            // Listen for remote users leaving the channel
            showMessage("Remote user offline $uid $reason")
        }

        override fun onLeaveChannel(stats: RtcStats) {
            // Listen for the local user leaving the channel
            isJoined = false
        }
    }


    private fun setupVoiceSDKEngine() {
        try {
            val config = RtcEngineConfig()
            config.mContext = baseContext
            config.mAppId = appId
            config.mEventHandler = mRtcEventHandler
            agoraEngine = RtcEngine.create(config)
        } catch (e: Exception) {
            throw RuntimeException("Check the error.")
        }
    }

    private fun joinChannel() {
        val options = ChannelMediaOptions()
        options.autoSubscribeAudio = true
        // Set both clients as the BROADCASTER.
        if (uid == 5) {
            options.clientRoleType = Constants.CLIENT_ROLE_BROADCASTER
        } else {
            options.clientRoleType = Constants.CLIENT_ROLE_AUDIENCE
        }
        // Set the channel profile as BROADCASTING.
        options.channelProfile = Constants.CHANNEL_PROFILE_LIVE_BROADCASTING

        // Join the channel with a temp token.
        // You need to specify the user ID yourself, and ensure that it is unique in the channel.
        agoraEngine?.joinChannel(token, channelName, uid, options)
    }

    fun joinLeaveChannel(view: View?) {
        if (isJoined) {
            agoraEngine?.leaveChannel()
        } else {
            joinChannel()
        }
    }

    private fun setupFastboard() {
        // Gets the fastboard instance
        val fastboardView = findViewById<FastboardView>(R.id.white)
        val fastboard = fastboardView.fastboard

        // Sets room configuration parameters
        val roomOptions = FastRoomOptions(
            "d0O6sF87Ee2BimmL04v3Jg/TbYqhlJXaTyKMQ",
            "c36ad7d0697611ed8e58650184ae5e98",
            "NETLESSROOM_YWs9MXZlakRxSGJkQkhHNll0TCZleHBpcmVBdD0xNjY5MDIzMDM5NzU3Jm5vbmNlPTE2NjkwMTk0Mzk3NTcwMCZyb2xlPTAmc2lnPTI5ZTZlNDdkNzMyMWYxNjE0NDM2ZTAzZWQ2MzQ1ZGE4OTUzZTQ3NmVmMTQwZjAwMjMyOGYwZjQ2NzQxODgzZDImdXVpZD1jMzZhZDdkMDY5NzYxMWVkOGU1ODY1MDE4NGFlNWU5OA",
            "c36ad7d0697611ed8e58650184ae5e98",
            FastRegion.US_SV
        )

        // Creates the FastRoom instance
        val fastRoom = fastboard.createFastRoom(roomOptions)
        // Joins the whiteboard room
        fastRoom.join()
    }

    private fun initListeners() {
        ZoomSDK.getInstance().meetingService.addListener(object : MeetingServiceListener {
            override fun onMeetingStatusChanged(meetingStatus: MeetingStatus, i: Int, i1: Int) {
                if (meetingStatus == MeetingStatus.MEETING_STATUS_INMEETING) {
                    joinChannel()
                }
            }

            override fun onMeetingParameterNotification(meetingParameter: MeetingParameter) {}
        })
        ZoomSDK.getInstance().inMeetingService.addListener(object : InMeetingServiceListener {
            override fun onShareMeetingChatStatusChanged(start: Boolean) {}
            override fun onMeetingNeedPasswordOrDisplayName(
                b: Boolean,
                b1: Boolean,
                inMeetingEventHandler: InMeetingEventHandler
            ) {
            }

            override fun onWebinarNeedRegister(s: String) {}
            override fun onJoinWebinarNeedUserNameAndEmail(inMeetingEventHandler: InMeetingEventHandler) {}
            override fun onMeetingNeedCloseOtherMeeting(handler: InMeetingEventHandler) {}
            override fun onMeetingFail(i: Int, i1: Int) {}
            override fun onMeetingLeaveComplete(l: Long) {
                agoraEngine?.leaveChannel()
            }

            override fun onMeetingUserJoin(list: List<Long>) {}
            override fun onMeetingUserLeave(list: List<Long>) {}
            override fun onMeetingUserUpdated(l: Long) {}
            override fun onMeetingHostChanged(l: Long) {}
            override fun onMeetingCoHostChanged(l: Long) {}
            override fun onMeetingCoHostChange(userId: Long, isCoHost: Boolean) {}
            override fun onActiveVideoUserChanged(l: Long) {}
            override fun onActiveSpeakerVideoUserChanged(l: Long) {}
            override fun onSpotlightVideoChanged(b: Boolean) {}
            override fun onSpotlightVideoChanged(userList: List<Long>) {}
            override fun onUserVideoStatusChanged(
                l: Long,
                videoStatus: InMeetingServiceListener.VideoStatus
            ) {
            }

            override fun onUserNetworkQualityChanged(l: Long) {}
            override fun onSinkMeetingVideoQualityChanged(
                videoQuality: VideoQuality,
                userId: Long
            ) {
            }

            override fun onMicrophoneStatusError(mobileRTCMicrophoneError: MobileRTCMicrophoneError) {}
            override fun onUserAudioStatusChanged(
                l: Long,
                audioStatus: InMeetingServiceListener.AudioStatus
            ) {
            }

            override fun onHostAskUnMute(l: Long) {}
            override fun onHostAskStartVideo(l: Long) {}
            override fun onUserAudioTypeChanged(l: Long) {}
            override fun onMyAudioSourceTypeChanged(i: Int) {}
            override fun onLowOrRaiseHandStatusChanged(l: Long, b: Boolean) {}
            override fun onChatMessageReceived(inMeetingChatMessage: InMeetingChatMessage) {}
            override fun onChatMsgDeleteNotification(
                msgID: String,
                deleteBy: ChatMessageDeleteType
            ) {
            }

            override fun onSilentModeChanged(b: Boolean) {
            }

            override fun onFreeMeetingReminder(b: Boolean, b1: Boolean, b2: Boolean) {}
            override fun onMeetingActiveVideo(l: Long) {}
            override fun onSinkAttendeeChatPriviledgeChanged(i: Int) {}
            override fun onSinkAllowAttendeeChatNotification(i: Int) {}
            override fun onSinkPanelistChatPrivilegeChanged(privilege: MobileRTCWebinarPanelistChatPrivilege) {}
            override fun onUserNameChanged(l: Long, s: String) {}
            override fun onUserNamesChanged(userList: List<Long>) {}
            override fun onPermissionRequested(permissions: Array<String>) {}
            override fun onFreeMeetingNeedToUpgrade(
                freeMeetingNeedUpgradeType: FreeMeetingNeedUpgradeType,
                s: String
            ) {
            }

            override fun onFreeMeetingUpgradeToGiftFreeTrialStart() {}
            override fun onFreeMeetingUpgradeToGiftFreeTrialStop() {}
            override fun onFreeMeetingUpgradeToProMeeting() {}
            override fun onClosedCaptionReceived(message: String, senderId: Long) {}
            override fun onRecordingStatus(recordingStatus: RecordingStatus) {}
            override fun onInvalidReclaimHostkey() {}
            override fun onHostVideoOrderUpdated(orderList: List<Long>) {}
            override fun onFollowHostVideoOrderChanged(bFollow: Boolean) {}
            override fun onAllHandsLowered() {}
            override fun onLocalRecordingStatus(userId: Long, status: RecordingStatus) {}
            override fun onLocalVideoOrderUpdated(localOrderList: List<Long>) {}
        })
    }

    override fun onStartShare() {

    }

    override fun onStopShare() {
    }

    override fun onDestroy() {
        super.onDestroy()
        whiteboardView.removeAllViews()
//        whiteboardView.destroy()
        agoraEngine?.leaveChannel();

        // Destroy the engine in a sub-thread to avoid congestion
        Thread {
            RtcEngine.destroy()
            agoraEngine = null
        }.start()
    }

    fun showMessage(message: String?) {
        runOnUiThread {
            Toast.makeText(
                applicationContext,
                message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
